**pizaioli_front**
pour pouvoir utiliser ce projet, il faut au préalable avoir installé nodeJS
faire la commande "npm install" pour installer les modules nécessaires
HOME("/"):
logo: couleur de l'Italie, redirection à la page HOME
caroussel :diffusion et choix d'images
bouton "COMMANDEZ NOS PIZZAS" => redirection à la page carte
API GOOGLE: localisation du magasin
liens dans la barre de navigation:
- LACARTE => redirection à la page carte
- CONNEXION => se connecter à son compte, affichage d'une fenêtre succés ou non
- DECONNEXION => déconnection et suppression token, affichage d'une fenêtre détaillant l'action
- PROFIL => redirection espace membre
CARTE("/lacarte"):
choix des pizzas reçus par la BDD
33cm => possibilité d'ajouter une ou plusieurs pizzas au panier
FILTRER PAR CATEGORIE => filtrer les pizzas par catégorie(blanches/rouges)
PANIER:
intégré à droite de la CARTE
produits qui se rajoutent en temps réel par rapport au choix faits sur la CARTE
calcul du prix du panier
X => possibilité de supprimer une ou plusieurs pizzas
CHOISSISSEZ L'HEURE => choix de l'heure de livraison suivant les créneaux horaires disponibles en BDD
MESSAGE DE LIVRAISON => ajout ou non d'un message particulier
PASSER LA COMMANDE => finaliser la commande:
- envoi commande en BDD
- suppression visuelle de la commande effectué
- message indiquant la réussite de la commande
LOGIN("/register"):
rentrer informations afin de crér un compte utilisateur
inscription données dans la BDD si succés
affichage message de succés ou non, si succés redirection vers CARTE
PROFIL("/profil"):
affichage du profil du membre:
-nom, pénom, addresse, mail
COMMANDES => choisir une commande passée par l'utilisateur:
-affichage détails commande(prix, pizza(s), total commande)
DECONNEXION("/logup"):
affichage message déconnexion, suppression token associé au compte
GESTION PREPARATION("/gestioncommandes"):
commmandes du jour affichées par heure de livraison prévue
numéro de commande
le client ciblé
le type de pizza
le message
un bouton en attent qui change statut si on clique dessus
il passe en préparation
en livraison qui grise la commande sélectionnée


**piz'aioli back api**
SYNOPSYS :
Le projet dont nous avons hérité est Piz’Aioli.
Le projet consistait globalement, à créer un site pour une pizzeria afin que les internautes puissent commander avec ou sans compte, et offrir une interface admin au pizzaiolo afin qu’il puisse modifier la carte, les prix, les commandes, etc…
Bonus :

Gestion d’une plage horaire de commandes pour les clients
Suivi des commandes du jour par l’admin avec changement de statut
Système de fidélité
Géolocalisation avec temps de livraison
Commandes hebdomadaires.

Tâches effectuées :
pour le USER :

fait : register, login, commande, vue profil, accès à la carte, post panier commande avec heure en fonction du planning, carroussel
non fait : commande sans login + recup local storage, différents formats de pizzas, register caractères spéciaux, notif client changement état commande
bonus fait : gestion de l'heure de livraison des commandes en fonction du planning
bonus non fait : système fidélité, promotions, durée livraison API google maps
Pour l'ADMIN :
fait : register, login, commande, vue profil, accès à la carte, post panier, CRUD complet, carroussel
bonus fait : post panier commande avec heure en fonction du planning, suivi préparation du jour avec état des commandes

Temps estimé pour finir l'ensemble des fonctionnalités : 1 semaine
Utilisation des outils : GIT, Trello, SQL Workbench, Symfony, React, Redux, Bootstrap, PHP, Javascript
Informations concernant le CRUD :
Pour accéder au CRUD, vous devez vous connecter en tant qu’ADMIN et cliquer sur l’onglet administration de la navbar. Si vous souhaitez revenir au site, cliquez sur le bouton « retour au site commercial ».
Vous avez accès à plusieurs onglets :

Utilisateurs : create, edit, put, delete
Comptes : create, edit, put, delete
Catégories : create, edit, put, delete
Tailles : create, edit, put, delete
Ingrédients : create, edit, put, delete
Commandes : create, edit, put, delete
Panier : create, edit, put, delete
Pizzas : create, edit, put, delete
Recettes : create, edit, put, delete
Prix : create, edit, put, delete
Etats cmd : create, edit, put, delete
Logout : pour vous déconnecter

Dans le CRUD, il faut respecter les foreigner keys.
Diverses sécurités sont mises en place :

Vous ne pouvez pas supprimer un compte si celui-ci est lié à au moins une commande. Supprimer d’abord ses commandes.
Vous ne pouvez pas supprimer une catégorie si celle-ci est lié à au moins une pizza. Affecter d’abord les pizzas à une autre catégorie.
Vous ne pouvez pas supprimer un format si celui-ci est lié à au moins une pizza. Affecter d’abord les pizzas à un autre format.
Vous ne pouvez pas supprimer un ingrédient si celui-ci est lié à au moins une receipe. Modifier ou supprimer d’abord les recettes contenant cet ingrédient.
Vous ne pouvez pas supprimer une commande si celle-ci contient un panier. Supprimer d’abord le panier.
Vous ne pouvez pas supprimer une pizza si celle-ci est contenue dans un panier, ou si une recette est créée en rapport avec cette pizza. Supprimer d’abord le panier, puis supprimer les lignes de recette contenant l’id de cette pizza.
Vous ne pouvez pas supprimer un prix  si celui-ci appartient à une pizza. Modifier d’abord le prix des pizzas appartenant à cet id prix
Vous ne pouvez pas supprimer un état si celui-ci appartient à une commande. Modifier d’abord l’état des commandes contenant cet id état

Pour les insertions, remplissez simplement les formulaires. Quelques spécificités :

Possibilité de modifier commande via l’admin
Pour créer une commande (normalement non utile), aller dans commandes->ajouter , puis remplissez les formulaires, avec un numéro de commande différent de ceux existant , et un prix aléatoire ; puis créer un panier avec un numéro de commande correspondant à la commande précédemment créée, et entrez autant de lignes nécessaire pour chaque pizza.
Pour créer une nouvelle pizza, -> pizzas->ajouter-> choisissez un nom, une image, une categorie et un prix. Si vous voulez définir les ingrédients de cette pizza, créez une recette.
Pour créer une recette -> recette->ajouter, puis ajouter autant de lignes que d’ingrédients nécessaires.

API :
Au niveau des controllers, qui sont construits pour échanger une API avec le front en REACT, nous avions tout préparé pour réaliser le CRUD en front, mais le manque de temps de développement sur REACT nous a contraint à utiliser un CRUD en back pour l’admin.
Les controllers basiques, sans foreigner keys sont :

State, format, ingredient, catégorie, prix : CRUD complet

Pour les autres :


Controller Pizza : post pizza avec nom de pizza + recette avec ingrédients qu’elle contient + format + prix (non utilisé) //  get pizza avec liste des ingrédients  et catégorie et prix //  put pizza+recette (non utilisé) // delete pizza + recette


Controller Order : get commande + panier +pizzas du panier + ingredients+ format + categorie + prix // post commande + panier + pizzas (avec catégorie, prix, format) // put commande // check slots + post slots pour gestion horaires de commande


Controller Account : get compte  + commandes + paniers + pizzas (complet) // post compte + put + delete


identifiants de connexion utiles :

login : admin@admin.com

mot de passe : admin

Afin de pouvoir lancer le back-end Symfony :

git init
git remote add origin + lien projet
git pull origin master
composer install
php bin/console doctrine:database:create
régler .env avec identifiants si nécessaire
importer le fichier sql pizaioli dans /database via PHPMyAdmin
lancer le serveur avec php bin/console server:run

Lien Trello du projet :
https://trello.com/b/o9YcS7o3/pizaioli